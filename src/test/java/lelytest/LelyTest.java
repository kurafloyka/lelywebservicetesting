package lelytest;

import com.google.gson.Gson;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import models.Datum;
import models.Example;
import org.junit.Assert;
import org.junit.Test;

import static io.restassured.RestAssured.given;

public class LelyTest extends BaseApi {


    @Test
    public void allIdIsNotNull() {


        Response response = given()
                .contentType("application/json")
                .accept(ContentType.JSON)
                //.log().all()
                .when()
                .request("GET")
                //.prettyPeek()
                .then()
                .assertThat().statusCode(200).extract().response();


        //LOGGER.info(response.asString());
        Gson gson = new Gson();
        Example example = gson.fromJson(response.asString(), Example.class);
        //LOGGER.info("Response : " + example.getMeta().getPagination().getTotal());

        for (Datum datum: example.getData()){

            Assert.assertNotNull("Value is not null : ", datum.getId());

        }

    }

    @Test
    public void checkUser(){
String bearerToken = "1db9c9b6c959682be7c96f74ca532c3cb0bd331f46b86a92602f8d319481b6f5";


        Datum datum=new Datum();
        datum.setEmail("umut@gmail.com");
        datum.setName("test");
        datum.setGender("male");
        datum.setStatus("active");

        Response response = given()
                .headers(
                "Authorization",
                "Bearer " + bearerToken,
                "Content-Type",
                ContentType.JSON,
                "Accept",
                ContentType.JSON)
                .body(datum)
                .log().all()
                .when()
                .request("POST").prettyPeek()
                .then()
                //.assertThat().statusCode(201)
                .extract()
                .response();
        LOGGER.info(response.asString());

    }
}
