package lelytest;

import io.restassured.RestAssured;
import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BaseApi {
    public static Logger LOGGER;
    @Before
    public void setup() {

        RestAssured.baseURI = "https://gorest.co.in/public/v1/users";
        LOGGER = LoggerFactory.getLogger(BaseApi.class);
    }
}
